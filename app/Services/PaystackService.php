<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 02/11/2020
 * Time: 6:10 AM
 */

namespace App\Services;

class PaystackService
{
    public static function initialise($settings, $payload){

        $postdata = array_merge($payload, ['callback_url' => route('paystack_callback', ['reference' => $payload['reference']])]);

        $url = "https://api.paystack.co/transaction/initialize";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer '.$settings['secret_key']->value,
            'Content-Type: application/json',
        ]);

        $response = curl_exec ($ch);

        if ($response === false) {
            throw new \Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
        }

        curl_close ($ch);

        return json_decode($response);
    }
}