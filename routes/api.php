<?php

Route::group(['prefix' => 'test'], function(){
    Route::any('callback/paystack', 'Api\PaystackController@testCallback');
});

Route::group(['prefix' => 'production'], function(){
    Route::any('callback/paystack', 'Api\PaystackController@productionCallback');
});