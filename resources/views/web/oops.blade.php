@extends('web.layout')
@section('content')

    <section class="pro-content empty-content">
        <div class="container">

            <div class="row">
                <div class="col-12">
                    <div class="pro-empty-page">
                        <h2 style="font-size: 150px;"><i class="fa fa-exclamation-circle"></i></h2>
                        <h1 >@lang('website.Oops')</h1>
                        <p>
                            @lang('website.Payment Error')
                            <a href="{{url('/view-order/'.session('orders_id'))}}" class="btn-link"><b>@lang('website.View Order Detail')</b></a>
                        </p>
                    </div>
                    </p>
                </div>
            </div>

        </div>
    </section>

@endsection
